/*
 * Media device discovery library
 *
 * Copyright (C) 2012 Ideas on board SPRL
 *
 * Contact: Laurent Pinchart <laurent.pinchart@ideasonboard.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MEDIA_ENUMERATE_H__
#define __MEDIA_ENUMERATE_H__

struct media_enumerate;

/**
 * @brief Create a new media device enumeration context.
 *
 * Media device enumeration contexts are used to enumerate media devices. They
 * store all context information required by enumeration and cache the
 * enumeration results for later access. Enumeration itself is performed by the
 * separate media_enumerate_scan() function.
 *
 * Enumeration contexts are reference-counted, see media_enumerate_ref() and
 * media_enumerate_unref() for more information.
 *
 * @return A pointer to the new media device enumeration context or NULL if
 * memory cannot be allocated.
 */
struct media_enumerate *media_enumerate_new(void);

/**
 * @brief Take a reference to the enumeration context.
 * @param media_enum - enumeration context instance.
 *
 * Media device enumeration contexts are reference-counted. Taking a reference
 * to an enumeration context prevents it and all the information it stores from
 * being freed until all references are released. The reference count is
 * initialized to 1 when the context is created.
 *
 * @return A pointer to @a media.
 */
struct media_enumerate *media_enumerate_ref(struct media_enumerate *media_enum);

/**
 * @brief Release a reference to the enumeration context.
 * @param media_enum - enumeration context instance.
 *
 * Release a reference to the enumeration context. When the reference count
 * reaches 0 this function frees the context, including all information it
 * stores.
 */
void media_enumerate_unref(struct media_enumerate *media_enum);

/**
 * @brief Enumerate media devices in the system
 * @param media_enum - enumeration context instance.
 *
 * Scan the system to locate all available media devices. This function must be
 * called once and only once before accessing the list of enumerated devices.
 *
 * Devices are enumerated using libudev if available or through sysfs otherwise.
 * sysfs-based enumeration requires device nodes to use the standard Linux
 * device names and be numbered by the device sysfs device number. For instance
 * the device node corresponding to /sys/class/video4linux/video0 must be named
 * /dev/video0. libudev-based enumeration doesn't have this restriction.
 *
 * Media devices are enumerated by scanning media controller, V4L and ALSA
 * kernel devices, in that order. Emulated media devices are created as needed
 * for V4L devices that are not part of a media controller based device. ALSA
 * devices without a corresponding media controller or V4L devices are skipped.
 *
 * @return Zero on success or a negative error code on failure.
 */
int media_enumerate_scan(struct media_enumerate *media_enum);

/**
 * @brief Get the number of media devices found during enumeration
 * @param media_enum - enumeration context instance.
 *
 * This function returns the total number of media devices found in the system
 * during enumeration. If media devices haven't been enumerated yet it will
 * return 0.
 *
 * @return The number of media devices found in the system
 */
unsigned int media_enumerate_get_devices_count(struct media_enumerate *media_enum);

/**
 * @brief Get the media devices
 * @param media_enum - enumeration context instance.
 *
 * This function returns a pointer to the array of enumerated media devices. If
 * If media devices haven't been enumerated yet it will return NULL.
 *
 * The array of media devices is owned by the enumeration context and will be
 * freed when the enumeration context is destroyed.
 *
 * @return A pointer to an array of media devices
 */
struct media_device **media_enumerate_get_devices(struct media_enumerate *media_enum);

#endif	/* __MEDIA_ENUMERATE_H__ */
