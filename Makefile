CROSS_COMPILE ?=

CC	:= $(CROSS_COMPILE)gcc
CFLAGS	?= -O2 -W -Wall -I$(HOME)/.local/include
LDFLAGS	?= -L$(HOME)/.local/lib64
LIBS	:= -lmediactl -lv4l2subdev -ludev

%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $<

all: media-enum

media-enum: main.o media-enumerate.o
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	-rm -f *.o
	-rm -f media-enum

